# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.1.54-1ubuntu4
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2012-03-30 01:47:05
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for jobforcareer
CREATE DATABASE IF NOT EXISTS `jobforcareer` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jobforcareer`;


# Dumping structure for table jobforcareer.company
CREATE TABLE IF NOT EXISTS `company` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `no_fax` varchar(20) DEFAULT NULL,
  `email_cp` varchar(35) NOT NULL,
  `email_event` varchar(35) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.company: ~1 rows (approximately)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` (`id`, `user_id`, `nama`, `alamat`, `no_telp`, `no_fax`, `email_cp`, `email_event`) VALUES
	(1, 11, 'omenk corporate', 'jogja', '098989898', '098989898', 'omenkzz@gmail.com', 'omenkzz@yahoo.com');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;


# Dumping structure for table jobforcareer.jobpost
CREATE TABLE IF NOT EXISTS `jobpost` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_company` int(5) DEFAULT NULL,
  `title` varchar(35) NOT NULL,
  `deskripsi` text,
  `jenkel` char(1) NOT NULL,
  `pendidikan_terakhir` varchar(50) NOT NULL,
  `umur_min` int(2) DEFAULT NULL,
  `umur_max` int(2) DEFAULT NULL,
  `min_pengalaman` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_company` (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.jobpost: ~19 rows (approximately)
/*!40000 ALTER TABLE `jobpost` DISABLE KEYS */;
INSERT INTO `jobpost` (`id`, `id_company`, `title`, `deskripsi`, `jenkel`, `pendidikan_terakhir`, `umur_min`, `umur_max`, `min_pengalaman`) VALUES
	(1, 11, 'Java/Php Developer ', 'tidak ada', 'w', 'Sarjana', 22, 35, 5),
	(2, 11, 'HRD', 'tidak ada', 'p', 's1', 22, 55, 2),
	(5, 11, 'game programmer', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(6, 11, 'programmer C', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(7, 11, 'programmer C', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(8, 11, 'programmer C', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(9, 11, 'programmer C', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(14, 11, 'python', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(15, 11, 'ruby on rails', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(16, 11, 'ruby on rails', 'requirement :', 'p', 'strata 1', 15, 25, 2),
	(17, 11, 'node.js', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(18, 11, 'extjs', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(19, 11, 'extjs 4', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(20, 11, 'play framework', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(21, 11, 'scala', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(22, 11, 'Spring MVC', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(23, 11, 'html', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(24, 11, 'html', 'requirementnya', 'p', 'strata 1', 15, 25, 2),
	(25, 11, 'cocos2d', 'requirementnya', 'p', 'strata 1', 15, 25, 2);
/*!40000 ALTER TABLE `jobpost` ENABLE KEYS */;


# Dumping structure for table jobforcareer.jobpost_jurusan
CREATE TABLE IF NOT EXISTS `jobpost_jurusan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_jobpost` varchar(50) DEFAULT NULL,
  `jurusan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_jobpost` (`id_jobpost`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='detail jurusan di jobpost';

# Dumping data for table jobforcareer.jobpost_jurusan: 2 rows
/*!40000 ALTER TABLE `jobpost_jurusan` DISABLE KEYS */;
INSERT INTO `jobpost_jurusan` (`id`, `id_jobpost`, `jurusan`) VALUES
	(12, '25', 'Sistem Informasi'),
	(11, '25', 'Tekhnik Informatika');
/*!40000 ALTER TABLE `jobpost_jurusan` ENABLE KEYS */;


# Dumping structure for table jobforcareer.jobseeker
CREATE TABLE IF NOT EXISTS `jobseeker` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `id_jobseeker` varchar(8) NOT NULL,
  `jenis_identitas` char(25) NOT NULL,
  `no_identitas` char(25) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `hp` char(13) NOT NULL,
  `jenkel` char(9) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `cv` varchar(255) DEFAULT NULL,
  `universitas` varchar(35) NOT NULL,
  `jenjang` varchar(35) NOT NULL,
  `jurusan` varchar(35) NOT NULL,
  `nilai` varchar(35) NOT NULL,
  `tahun_masuk` int(11) NOT NULL,
  `tahun_lulus` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_jobseeker` (`id_jobseeker`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.jobseeker: ~2 rows (approximately)
/*!40000 ALTER TABLE `jobseeker` DISABLE KEYS */;
INSERT INTO `jobseeker` (`id`, `id_jobseeker`, `jenis_identitas`, `no_identitas`, `nama`, `hp`, `jenkel`, `tempat_lahir`, `tanggal_lahir`, `foto`, `cv`, `universitas`, `jenjang`, `jurusan`, `nilai`, `tahun_masuk`, `tahun_lulus`) VALUES
	(1, 's1s1s2', 'KTP', '12312312312', 'Saksomo Herwijaya', '812963891263', 'p', 'yogya', '2012-03-14', NULL, NULL, 'pelita nusantara', 'S1', 'Teknik Informatika', '5', 2008, 2009),
	(2, 'CAC83796', 'ktp', '09898987', 'kurnia muhamad', '0989897', 'P', 'masasuset', '1986-03-19', 'photo1.jpg', 'OO_tes1.pdf', 'MIT', 'S2', 'Physics', '3.5', 2000, 2000);
/*!40000 ALTER TABLE `jobseeker` ENABLE KEYS */;


# Dumping structure for table jobforcareer.lamaran
CREATE TABLE IF NOT EXISTS `lamaran` (
  `id_company` int(10) DEFAULT NULL,
  `id_jobseeker` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `action` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.lamaran: ~0 rows (approximately)
/*!40000 ALTER TABLE `lamaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `lamaran` ENABLE KEYS */;


# Dumping structure for table jobforcareer.member_card
CREATE TABLE IF NOT EXISTS `member_card` (
  `id_barcode` varchar(10) NOT NULL DEFAULT '0',
  `id_jobseeker` varchar(8) NOT NULL,
  `id_petugas` int(10) NOT NULL,
  `isactive` char(5) NOT NULL,
  PRIMARY KEY (`id_barcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.member_card: ~2 rows (approximately)
/*!40000 ALTER TABLE `member_card` DISABLE KEYS */;
INSERT INTO `member_card` (`id_barcode`, `id_jobseeker`, `id_petugas`, `isactive`) VALUES
	('0', '1', 1, 'true'),
	('321312', 's1s1s2', 1, 'true');
/*!40000 ALTER TABLE `member_card` ENABLE KEYS */;


# Dumping structure for table jobforcareer.pengalaman_kerja
CREATE TABLE IF NOT EXISTS `pengalaman_kerja` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `jobseeker_id` int(15) NOT NULL,
  `jabatan` varchar(35) NOT NULL,
  `tempat_bekerja` varchar(35) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`),
  KEY `jobseeker_id` (`jobseeker_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.pengalaman_kerja: ~1 rows (approximately)
/*!40000 ALTER TABLE `pengalaman_kerja` DISABLE KEYS */;
INSERT INTO `pengalaman_kerja` (`id`, `jobseeker_id`, `jabatan`, `tempat_bekerja`, `date_start`, `date_end`, `keterangan`) VALUES
	(1, 2, 'Programmer', 'Google Inc', '1999-03-11', '2012-03-04', 'tidak ada keterangan ki bung');
/*!40000 ALTER TABLE `pengalaman_kerja` ENABLE KEYS */;


# Dumping structure for table jobforcareer.petugas
CREATE TABLE IF NOT EXISTS `petugas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `hp` char(13) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.petugas: ~4 rows (approximately)
/*!40000 ALTER TABLE `petugas` DISABLE KEYS */;
INSERT INTO `petugas` (`id`, `user_id`, `nama`, `alamat`, `hp`) VALUES
	(1, 1, '1', 'ashdkaslhdkla', '982689689'),
	(7, 8, 'Nama Kasir', 'adasdasdasdasdas', '12312321'),
	(8, 9, 'pintu masuk', 'pintumasukpintumasuk', '13213'),
	(9, 10, 'nama pintu kleuar', 'ajsdghasgdasghdj', '21312');
/*!40000 ALTER TABLE `petugas` ENABLE KEYS */;


# Dumping structure for table jobforcareer.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(10) NOT NULL DEFAULT '0' COMMENT '1=SUPER ADMIN, 2=KASIR, 3=PINTU MASUK, 4=PINTU KLUAR, 5=Company',
  `registered` date DEFAULT NULL,
  `lastlogin` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

# Dumping data for table jobforcareer.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `level`, `registered`, `lastlogin`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '0000-00-00', NULL),
	(8, 'kasir', 'c7911af3adbd12a035b289556d96470a', '2', '0000-00-00', NULL),
	(9, 'pintumasuk', '79f9c52ab530b36ad8fd99bfeaf15ed6', '3', '0000-00-00', NULL),
	(10, 'pintukelua', 'c2b44957678bf7687a81d1847d0b9427', '4', '0000-00-00', NULL),
	(11, 'omenk', 'eeca72f674398ff7c344ff481e3482b5', 'company', '0000-00-00', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
