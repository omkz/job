# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.1.54-1ubuntu4
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2012-03-20 15:24:03
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for jobforcareer
CREATE DATABASE IF NOT EXISTS `jobforcareer` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jobforcareer`;


# Dumping structure for table jobforcareer.company
CREATE TABLE IF NOT EXISTS `company` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `hp` char(13) NOT NULL,
  `email_cp` varchar(35) NOT NULL,
  `email_event` varchar(35) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table jobforcareer.jobpost
CREATE TABLE IF NOT EXISTS `jobpost` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_company` int(5) DEFAULT NULL,
  `title` varchar(35) NOT NULL,
  `deskripsi` text,
  `posisi` varchar(35) NOT NULL,
  `jenkel` char(1) NOT NULL,
  `pendidikan_terakhir` varchar(50) NOT NULL,
  `umur_min` int(2) DEFAULT NULL,
  `umur_max` int(2) DEFAULT NULL,
  `min_pengalaman` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table jobforcareer.jobseeker
CREATE TABLE IF NOT EXISTS `jobseeker` (
  `no_identitas` char(25) NOT NULL,
  `jenis_identitas` char(25) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `hp` char(13) NOT NULL,
  `gender` char(1) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `foto` varchar(35) DEFAULT NULL,
  `pendidikan_terakhir` varchar(35) NOT NULL,
  `pengalaman_kerja_id` int(5) NOT NULL,
  PRIMARY KEY (`no_identitas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table jobforcareer.lamaran
CREATE TABLE IF NOT EXISTS `lamaran` (
  `id_company` int(10) DEFAULT NULL,
  `id_jobseeker` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `action` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table jobforcareer.member_card
CREATE TABLE IF NOT EXISTS `member_card` (
  `id_barcode` int(10) NOT NULL DEFAULT '0',
  `id_event` int(10) NOT NULL,
  `id_petugas` int(10) NOT NULL,
  `isactive` char(5) NOT NULL,
  PRIMARY KEY (`id_barcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table jobforcareer.pengalaman_kerja
CREATE TABLE IF NOT EXISTS `pengalaman_kerja` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `jobseeker_id` int(15) NOT NULL,
  `jabatan` varchar(35) NOT NULL,
  `tempat_bekerja` varchar(35) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table jobforcareer.petugas
CREATE TABLE IF NOT EXISTS `petugas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `posisi` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `hp` char(13) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table jobforcareer.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(25) NOT NULL,
  `level` varchar(15) NOT NULL DEFAULT '0',
  `registered` date DEFAULT NULL,
  `lastlogin` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Data exporting was unselected.
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
